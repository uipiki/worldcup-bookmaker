class UsersController < ApplicationController
  layout 'login'

  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user, only: [:edit, :update]
  before_action :admin_user, only: :destroy
  
  def index
    @users = User.paginate(page: params[:page])
  end
  
  def new
    @user = User.new
  end
  
  def show
    @user = User.find(params[:id])
    @country = Country.all[0]
    @betting = Betting.where(user_id: @user.id).map {|data| [data.country_id,data.bet_point] }.to_h
    @winpoints = Winpoint.all.map {|data| [data.name, data.win_point] }.to_h
    @subtotal = to_subtotal(@country, @betting, @winpoints)
    @total = @subtotal.values.inject(:+)
  end
  
  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      # save success
      log_in @user
      if Betting.find_by(user_id: params[:id])
        redirect_to controller: 'betting', action: 'list'
      else
        flash[:success] = "Welcome to the Worldcup bookmaker!"
        flash[:user_id] = @user.id
        redirect_to controller: 'betting', action: 'new'
      end
    else
      render 'new'
    end
  end
  
  def update
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end
  
  def edit
   @user = User.find(params[:id])
  end
  
  private
    def user_params
      params.require(:user).permit(:group_id, :name, :password, :password_confirmation)
    end
    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
    
    def to_subtotal(country, bettings, winpoints)
      #Hash[country.names.collect {|c| [c,0]}]
      return Hash[country.names.collect {|name|
        [name, bettings[name] * winpoints[name]]
      }]
    end
end
