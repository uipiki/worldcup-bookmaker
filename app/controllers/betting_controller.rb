class BettingController < ApplicationController
  
  Total_point = 528
  List_count = 32
  Max_num = 32
  Min_num = 1
  
  def index
  end
  
  def new
    if flash[:user_id] != nil
      @country = Country.all[0]
      @user = User.find(flash[:user_id])
      @betting = Betting.new
      @points = {}
    else
      redirect_to controller: 'sessions', action: 'new'
    end
  end
  
  def create
    user_id = params["betting"]["user_id"]
    @user = User.find(user_id)
    points = point_list(params)
    error_messages = error_message(points)
    if error_messages.length == 0
      Betting.import betting_list(user_id, params)
      flash[:success] = "You are success to bet!!!"
      redirect_to @user
    else
      flash.now[:danger] = error_messages
      @country = Country.all[0]
      @betting = Betting.new
      @points = params
      render :new, :user_id => user_id
    end
  end
  
  def list
  end
  
  def rank
  end
  
  private
  
    def error_message(points)
      messages = []
      if !total?(points, Total_point)
        messages.push("Error! total is not " + Total_point)
      end
      if duplicate?(points)
        messages.push("Error! duplicate value exists!")
      end
      if has_over?(points, Max_num)
        messages.push("Error! max is " + Max_num)
      end
      if has_less?(points, Min_num)
        messages.push("Error! min is " + Min_num)
      end
      return messages
    end

    def total?(pointList, total)
      return pointList.inject(:+) == total
    end
    
    def duplicate?(pointList)
      return pointList.uniq.length != List_count
    end
    
    def has_over?(point_list, max)
      return point_list.max > max
    end
    
    def has_less?(point_list, min)
      return point_list.min < min
    end
    
    def point_list(dict)
      return params.values.map{|val| 
      if val =~ /^[0-9]+$/
        val.to_i
      end
      }.compact
    end
    
    def betting_list(user_id, dict)
      return dict.map{|key,value|
      if value =~ /^[0-9]+$/
        Betting.new(user_id: user_id, country_id: key, bet_point: value)
      end
      }.compact
    end
end
