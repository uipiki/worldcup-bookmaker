class RankController < ApplicationController
    include Common
    
    def list
        user = view_context.current_user
        if user.nil?
            redirect_to controller: 'sessions', action: 'new'
        else
            @users = get_group_members(user.group_id)
            user_ids = @users.map { |user| user.id }
            @bettings = get_betting(user_ids)
            @win_point = win_point_dict()
            @country = Country.all[0]
            @sum_hash = get_sum_hash(@users, @bettings, @win_point)
            @user_betting_hash = Hash.new
            @users.each { |user|
                country_hash = Hash.new
                @bettings.where(user_id: user.id).each { |betting| 
                    country_hash[betting.country_id] = betting.bet_point
                }
                @user_betting_hash[user.name] = country_hash
            }
        end
    end
    
    def ranking
        user = view_context.current_user
        if user.nil?
            redirect_to controller: 'sessions', action: 'new'
        else
            @users = get_group_members(user.group_id)
            user_ids = @users.map { |user| user.id }
            @ranked_users_with_point = get_sum_hash(@users, get_betting(user_ids), win_point_dict()).sort_by{ | k, v | v }.reverse
            @ranking_list = Array.new
            lastpoint = 0
            top_point = @ranked_users_with_point[0][1]
            @ranked_users_with_point.each {|user_point|
                rank_hash = Hash.new
                user = user_point[0]
                point = user_point[1]
                rank_hash["name"] = user
                rank_hash["point"] = point
                rank_hash["diffs"] = lastpoint - point 
                rank_hash["to_the_top"] = top_point - point
                @ranking_list.push(rank_hash)
            }
        end
    end
    
    private
        def get_group_members(group_id)
            return User.where(group_id: group_id)
        end
        
        def get_betting(user_ids)
            return Betting.where(user_id: user_ids).order("user_id")
        end
        
        def get_win_point()
            return Winpoint.all()
        end
        
        def get_sum_hash(users, bettings, win_point)
            sum_hash = Hash.new
            users.each {|user|
                id = user.id
                sum = 0
                bettings.where(user_id: id).each { |betting|
                    sum = sum + betting.bet_point * win_point[betting.country_id]
                }
                sum_hash[user.name] = sum
            }
            return sum_hash
        end
        
end
