module Common
  extend ActiveSupport::Concern

  included do
    # ここにcallback等
  end

  # メソッド
  def categories
    ArticleCategory.order(:id).all
  end
  
  def echo(param)
      p "test success"
  end
      
  def is_num(value)
      return value =~ /^[0-9]+$/
  end
  
  def win_point_dict()
    win_point = Winpoint.all
    country = Country.all[0]
    if win_point.length > 0
      return Hash[win_point.map {|win| [win.name, win.win_point]}]
    else
      return Hash[country.names.collect {|c| [c,0]}]
    end
  end

  private

  # privateメソッド

end