class SessionsController < ApplicationController
  layout 'login'
  
  def new
  end
  
  def create
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      log_in user
      if Betting.find_by(user_id: user.id)
        redirect_to controller: 'rank', action: 'list'
      else
        flash[:success] = "Welcome to the Worldcup bookmaker!"
        redirect_to controller: 'betting', action: 'new'
      end
    else
      flash.now[:danger] = 'Invalid name/password combination' 
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
