class WinpointController < ApplicationController
  include Common
  
  def winpoint
    if current_user.name == "ui_h"
      @country = Country.all[0]
      @allpoint = Winpoint.all
      @points = win_point_dict()
      @winpoint = Winpoint.new
    else
      redirect_to controller: 'sessions', action: 'new'
    end
  end
  
  def update
    is_first = Winpoint.all.length == 0
    
    records = params.map{|key, value|
      if is_num(value)
        upsert(key, value, is_first)
      end
    }.compact
    if is_first
      Winpoint.import records
    else
      records.each {|record|
        record.save
      }
      p records
    end
  end
  
  private
    
    def upsert(name, point, is_first)
      if is_first
        insert_record(name, point)
      else
        update_record(name, point)
      end
    end
    
    def insert_record(name, point)
      return Winpoint.new(name: name, win_point: point)
    end
    
    def update_record(name, point)
      winpoint = Winpoint.find_by(name: name)
      if winpoint.win_point != point.to_i
        p "update record"
        winpoint.win_point = point
        return winpoint
      end
    end
end
