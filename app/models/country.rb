require 'active_model'

class Country < ActiveYaml::Base
    set_root_path "app/models/data"
    set_filename "country"
end