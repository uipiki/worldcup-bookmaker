Rails.application.routes.draw do
  resources :groups
  get 'sessions/new'
  root 'sessions#new'
  get '/rule', to: 'static_pages#rule'
  get '/about', to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get '/list', to: 'static_pages#list'
  get  '/signup',  to: 'users#new'
  post '/signup', to: 'users#create'
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  get '/betting', to: 'betting#new'
  post '/betting', to: 'betting#create'
  delete '/logout', to: 'sessions#destroy'
  get '/winpoint', to: 'winpoint#winpoint'
  post '/winpoint', to: 'winpoint#update'
  get '/groups', to: 'groups#new'
  get '/point_list', to: 'rank#list'
  get '/ranking', to: 'rank#ranking'
  resources :users
end
