class CreateBettings < ActiveRecord::Migration[5.0]
  def change
    create_table :bettings do |t|
      t.integer :user_id
      t.string :country_id
      t.integer :bet_point
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
