class CreateWinpoints < ActiveRecord::Migration[5.0]
  def change
    create_table :winpoints do |t|
      t.string :name
      t.integer :win_point

      t.timestamps
    end
  end
end
