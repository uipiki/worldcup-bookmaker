class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.integer :group_id
      t.boolean :group_admin
      t.boolean :master

      t.timestamps
    end
  end
end
